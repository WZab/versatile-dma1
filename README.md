# Simple AXI4-Stream -> PCIe core for Virtex 7 #
This is a simple implementation of a core that transfers the data from AXI4-Stream interface to the PC via PCIe interface.
It also provides the AXI4-Lite interface for control registers implemented in FPGA.

The project has evolved from one of DMA engines described in http://dx.doi.org/10.1117/12.2280937 , the [original design](https://github.com/wzab/Z-turn-examples/tree/master/axi_dma_prj2) was connected to CPU via AXI bus. That one uses PCIe.

The project is built using the VEXTPROJ environment available at https://github.com/wzab/vextproj
and described in http://dx.doi.org/10.1117/12.2247944 .

Please note, that the "agwb" branch contains additionally the Wishbone bus with periferials managed vie the AGWB ( https://github.com/wzab/agwb ) system described in https://doi.org/10.1117/12.2536259 .
