#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include "../driver/ax_dma1.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>


const char dev_name[]="/dev/ax1dma0";

volatile uint32_t * buf[BUF_NUM];

int main(int argc, char * argv[])
{
    int fdev;
    int res;
    int i, j, nbuf, nbytes, nwords;
    int buf_len;
    uint64_t rate;
    volatile uint32_t * regs;
    uint32_t val,val2, val3;
    time_t tstart, tprev, tnow = 0;
    uint64_t tbytes = 0;
    //Open our device
    fdev=open(dev_name,O_RDWR);
    if(fdev==-1) {
        perror("I can't open the device");
        exit(1);
    }
    printf("Opened the device\n");
    //Map the buffers
    for(i=0; i<BUF_NUM; i++) {
        buf[i]=mmap(0,BUF_SIZE,PROT_READ | PROT_WRITE, MAP_SHARED, fdev,i*0x1000);
        if(buf[i]==NULL) {
            perror("I can't map buffer");
            printf("buffer nr %d\n",i);
            exit(2);
        }
    }
    printf("Mapped all buffers\n");
    val=ioctl(fdev,ADM_RESET,0);
    if(val<0) {
        perror("ADM_RESET ioctl error");
        exit(3);
    }
    printf("ADM_RESET done\n");
    //Sleep 1 second
    sleep(1);
    val=ioctl(fdev,ADM_START,0);
    if(val<0) {
        perror("ADM_START ioctl error");
        exit(3);
    }
    printf("ADM_START done\n");
    tstart = time(NULL);
    tprev = tstart;
    while(1) {
        val = ioctl(fdev,ADM_GET,0);
        if(val<0) {
            perror("ADM_GET ioctl error");
            break;
        }
        //printf("ADM_GET done\n");
        nbuf = (val >> 24) & 0xf;
        nbytes = val & 0x3fffff;
        nwords = (nbytes+3)/4;
        if(0) {
            printf("received %d bytes in buffer %d\n",nbytes,nbuf);
            printf("%d: \n",0);
            printf("%x\n\n",buf[nbuf][0]);
            printf("%d: \n",1);
            printf("%x\n\n",buf[nbuf][1]);
            for(j=nwords-20; j<nwords; j++) {
                printf("%d: \n",j);
                printf("%x\n",buf[nbuf][j]);
            }
        }
        val = ioctl(fdev,ADM_CONFIRM,0);
        if(val<0) {
            perror("ADM_CONFIRM ioctl error");
            break;
        }
        //printf("ADM_CONFIRM done\n");
		tbytes += nbytes;
        tnow = time(NULL);
        if(tnow-tprev > 3) {
            tprev = tnow;
            rate = 8*tbytes / (tnow - tstart);
            printf("transmitted: %lld bytes in %lld seconds, rate: %lld b/s\n", tbytes, tnow-tstart, rate);
        }
    }
    val=ioctl(fdev,ADM_STOP,0);
    if(val<0) {
        perror("ADM_START ioctl error");
        exit(3);
    }
    printf("ADM_STOP done\n");
    //Trzeba będzie jeszcze dodać odmapowanie buforów!
    close(fdev);
    return 0;
}
