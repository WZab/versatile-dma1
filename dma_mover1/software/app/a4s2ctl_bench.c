#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
void main()
{
   int f, i,j;
   time_t tstart, tprev, tnow = 0;
   uint64_t ttrans = 0;
   uint64_t rate = 0;
   volatile uint32_t * m;
   uint32_t test_val = 0;
   uint32_t read_val = 0;
   f=open("/dev/ax1ctl0",O_RDWR);
   if(f<0) { 
           printf("I can't open control device!\n");
           exit(1);
	}
   m=(uint32_t *) mmap(NULL, 1024, PROT_READ | PROT_WRITE , MAP_SHARED,f,0);
   if(m==NULL) { 
           printf("I can't mmap registers!\n");
           exit(1);
	}
   printf("mapped!\n");
   tstart = time(NULL);
   tprev=tstart;
   while(1) {
     test_val += 0x12372131;
     m[0] = test_val;
     read_val = m[2];
     ttrans ++;
     if ((test_val ^ 0x12345678) != read_val) {
        printf("test: %x, read: %x, expected: %x\n",test_val, read_val, (test_val ^ 0x12345678) );
        break;     
     }
     tnow = time(NULL);
     if(tnow-tprev > 3) {
       tprev = tnow;
       rate = ttrans / (tnow - tstart);
       printf("transactions: %lld  in %lld seconds, rate: %lld trans/s\n", ttrans, tnow-tstart, rate);
     }     
/*
   m[1]=0xffffffff;
   for(j=0;j<32;j++) {
     m[0]=j;
     msync(&m[i],4,MS_SYNC);
     for(i=0;i<4;i++)
       printf("%d : %4.4x\n",i,m[i]);
   }
   */
   }
   for(j=0;j<32;j++) {
     m[0]=j;
     msync(&m[i],4,MS_SYNC);
     for(i=0;i<4;i++)
       printf("%d : %4.4x\n",i,m[i]);
   }
   munmap(m,1024);
}
