/* Quick and dirty WZAB PCIe Bus Mastering device driver
 * for Virtex
 * Copyright (C) 2019 by Wojciech M. Zabolotny
 * wzab<at>ise.pw.edu.pl
 * Significantly based on multiple drivers included in
 * sources of Linux
 * Therefore this source is licensed under GPL v2
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <asm/uaccess.h>
MODULE_LICENSE("GPL v2");
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/delay.h>
#include <linux/pci.h>
#include <asm/io.h>
#include <linux/interrupt.h>
#include <asm/uaccess.h>
#include "ax_dma1.h"


//PCI IDs below are not registred! Use only for experiments!
#define PCI_VENDOR_ID_WZAB 0x32ab
#define PCI_DEVICE_ID_WZAB_BM1 0x7025
//Address layout at the PCI side:
//RES0 - s_axi_ctl (256MB)
//RES1 - gpio_and_fifo (256MB)

//AXI FIFO REGISTERS
#define AF_STR_RESET 0x28
#define AF_TX_RESET 0x8
#define AF_RX_RESET 0x18
#define AF_ISR 0x00
#define AF_IER 0x04
#define AF_RDFD 0x20
#define AF_RDFO 0x1c
#define AF_TDFD 0x10
#define AF_TLR 0x14
#define AF_RLR 0x24

static const struct pci_device_id tst1_pci_tbl[] = {
    {PCI_VENDOR_ID_WZAB, PCI_DEVICE_ID_WZAB_BM1, PCI_ANY_ID, PCI_ANY_ID, 0, 0, 0 },
    {0,}
};
MODULE_DEVICE_TABLE(pci, tst1_pci_tbl);

#define SUCCESS 0
#define DEVICE_NAME "wzab_axs1"

#define N_OF_RES (3)
//Our device contains two BARs
//The first one is the PCI core register file
#define PCI_REGS (0)
//The second one is the GPIO and AXI_STREAM_FIFO area (with "start" GPIO at 0x10000)
#define GPIO_REGS (1)
//The third one is the LOCAL BUS
#define LBUS_REGS (2)


//Structure describing the status of the WZAB_AXS1 device
#define MY_MAX_DEVICES  8

typedef struct {
    struct pci_dev * pdev;
    int nr;
    int irq;
    int irq_set;
    uint32_t * gpio_regs;
    uint32_t * pci_regs;
    uint32_t * fifo_regs;
    resource_size_t mmio_start[N_OF_RES];
    resource_size_t mmio_end[N_OF_RES];
    resource_size_t mmio_flags[N_OF_RES];
    resource_size_t mmio_len[N_OF_RES];
    uint32_t * virt_buf[BUF_NUM];
    dma_addr_t phys_buf[BUF_NUM];
    volatile int blen[BUF_NUM];
    volatile int bready[BUF_NUM];
    int received_buf;
    volatile int exp_buf;
    int is_started;
} axs1_dev;

//Table for storing the device contexts
static axs1_dev * devs[MY_MAX_DEVICES]; //Initialized to NULL
DEFINE_MUTEX(devs_lock);
DECLARE_WAIT_QUEUE_HEAD (readqueue);

inline static void set_reset(axs1_dev * adev, int val)
{
    uint32_t tmp = adev->gpio_regs[0];
    if (val)
        tmp |= 1;
    else
        tmp &= 0xffffFFFF - 1;
    adev->gpio_regs[0] = tmp;
    mb();
}

inline static void set_start(axs1_dev * adev, int val)
{
    uint32_t tmp = adev->gpio_regs[0];
    if (val)
        tmp |= 2;
    else
        tmp &= 0xffffFFFF - 2;
    adev->gpio_regs[0] = tmp;
    mb();
}

inline static axs1_dev * axs1_dev_alloc( void )
{
    axs1_dev * adev = kzalloc(sizeof(axs1_dev), GFP_KERNEL);
    return adev;
}

inline static void axs1_dev_free(axs1_dev * adev)
{
    int i;
    if(adev->pdev) { //adev is linked, so check for resources, that must be freed
        if(adev->gpio_regs) pci_iounmap(adev->pdev,adev->gpio_regs);
        if(adev->pci_regs) pci_iounmap(adev->pdev,adev->pci_regs);
        for(i=0 ; i < BUF_NUM ; i++ ) {
            if(adev->virt_buf[i]) dma_free_coherent(&adev->pdev->dev, BUF_SIZE, adev->virt_buf[i], adev->phys_buf[i]);
        }
    }
    kfree(adev);
}

void cleanup_tst1( void );
void cleanup_tst1( void );
int init_tst1( void );
static int tst1_open(struct inode *inode, struct file *file);
static int tst1_release(struct inode *inode, struct file *file);
static long tst1_ioctl(struct file * fd, unsigned int cmd, unsigned long arg);

int tst1_mmap(struct file *filp, struct vm_area_struct *vma);

static dev_t my_dev_t = 0;
static struct cdev * my_cdev = NULL;
static struct class *class_my_tst = NULL;

static dev_t my_dev_ctl_t = 0;
static struct cdev * my_cdev_ctl = NULL;
static struct class *class_my_ctl = NULL;

struct file_operations Fops = {
    .owner = THIS_MODULE,
    //.read=tst1_read, /* read */
    //.write=tst1_write, /* write */
    .open=tst1_open,
    .unlocked_ioctl = tst1_ioctl,
    .release=tst1_release,  /* a.k.a. close */
    //.llseek=no_llseek,
    .mmap=tst1_mmap
};

static int ctl1_open(struct inode *inode, struct file *file);
static int ctl1_release(struct inode *inode, struct file *file);
int ctl1_mmap(struct file *filp, struct vm_area_struct *vma);

struct file_operations FopsCtl = {
    .owner = THIS_MODULE,
    //.read=tst1_read, /* read */
    //.write=tst1_write, /* write */
    .open=ctl1_open,
    .release=ctl1_release,  /* a.k.a. close */
    //.llseek=no_llseek,
    .mmap=ctl1_mmap
};

static int ctl1_open(struct inode *inode,
                     struct file *file)
{
    int nr = MINOR(inode->i_rdev);
    axs1_dev * adev = devs[nr];
    file->private_data = adev;

    nonseekable_open(inode, file);
    return SUCCESS;
}
static int ctl1_release(struct inode *inode,
                        struct file *file)
{
    file->private_data = NULL;
    return SUCCESS;
}

int ctl1_mmap(struct file *filp,
              struct vm_area_struct *vma)
{
    unsigned long vsize;
    unsigned long psize;
    unsigned long physical;
    axs1_dev * adev = NULL;
    unsigned long off = vma->vm_pgoff << PAGE_SHIFT;
    adev = (axs1_dev *) filp->private_data;
    //Currently we provide access only to the GPIOS1
    physical = adev->mmio_start[LBUS_REGS];
    psize = 1L << 32; //4GB!
    vsize = vma->vm_end - vma->vm_start;
    if( vsize + off > psize ) {
        return -EINVAL;
    }
    //Disable caching
    vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
    if(remap_pfn_range(vma,vma->vm_start, physical >> PAGE_SHIFT,
                       vsize, vma->vm_page_prot)) {
        return -EAGAIN;
    }
    return 0;
}


irqreturn_t tst1_irq(int irq, void * dev_id)
{
    int res;
    int nbuf;
    volatile uint32_t * regs;
    axs1_dev * adev = (axs1_dev *) dev_id;
    regs = (volatile uint32_t *) adev->fifo_regs;
    // First we check if our device requests interrupt
    //printk("<1>I'm in interrupt!\n");
    //It is good, because in PCIe it eliminates the race...
    if(regs[AF_ISR/4] & 0x04000000) {
        //Yes, this is our interrupt
        regs[AF_ISR/4] = 0xffffffff; //Clear interrupts why all!?
        mb();
        //printk( KERN_INFO "OK. In inerrupt\n");
        while(1) {
            res = regs[AF_RDFO/4]; //FIFO occupancy
            if(res==0) break; //No more packets to receive
            res = regs[AF_RLR/4];
            if(res != 4) {
                //This is an incorrect response, how we can handle it?
                printk( KERN_ERR "Incorrect length of AXI DM status: %d, but should be 4\n",res);
                break; //Should we really leave? How to restore proper operation?
            }
            //res==4 so we simply read the status word
            res = regs[AF_RDFD/4];
            //This is the status word. Bits 0-3 TAG, bit 4 - INTERR, bit 5 - DECERR, bit 6 - SLVERR
            //bit 7 - OKAY, bits 30-8 - length of the transfer, bit 31 - EOP
            if((res & (1<<7))==0) {
                printk( KERN_ERR "Incorrect status of AXI_DM status packet, should be OKAY, value is %x\n",res);
                break; //Again - what should be the correct action?
            }
            //Decode the number of the handled buffer
            nbuf = res & 0xf;
            //Here we should verify, that this is an expected buffer
            if(nbuf != adev->exp_buf) {
                printk( KERN_ERR "Incorrect buffer number. Expected: %x, received: %x\n", adev->exp_buf, nbuf);
            }
            //Decode the number of received bytes
            adev->blen[nbuf] = (res & 0x3fffff00) >> 8;
            //Mark the buffer as ready
            adev->bready[nbuf]=1;
            //Update the number of the expected buffer
            adev->exp_buf += 1;
            if(adev->exp_buf == BUF_NUM) adev->exp_buf = 0;
        }
        //Wake up the reading process
        wake_up_interruptible(&readqueue);
        return IRQ_HANDLED;
    }
    return IRQ_NONE; //Our device does not request interrupt
};


/* Function requesting transfer of the i-th buffer */
static void transfer_buf(axs1_dev * adev, int i)
{
    volatile uint32_t * regs;
    uint32_t val;
    regs = (volatile uint32_t *) adev->fifo_regs;
    val = (1<<22)-1 ; //Maximum length of the transfer
    val |= (1<<23);
    val |= (1<<30);
    //Write it to the FIFO
    regs[AF_TDFD/4] = val;
    mb();
    val = adev->phys_buf[i] & 0xffffffff;
    regs[AF_TDFD/4] = val;
    mb();
    val = adev->phys_buf[i] >> 32;
    regs[AF_TDFD/4] = val;
    mb();
    val = i;
    regs[AF_TDFD/4] = val;
    mb();
    regs[AF_TLR/4] = 13; //Our command is only 13 bytes long
    mb();
}

long tst1_ioctl(struct file * fd, unsigned int cmd, unsigned long arg)
{
    int i, res;
    volatile uint32_t * regs;
    axs1_dev * adev = (axs1_dev *) fd->private_data;
    regs = (volatile uint32_t *) adev->fifo_regs;
    switch(cmd) {
    case ADM_RESET:
        //We reset the DataMover - it requires hardware support!
        //OK. It is done via the GPIO. However now I want to control that GPIO myself. Not via the standard driver!
        //bit 0 - reset of the DMA engine
        //bit 1 - start of the data generator
        //WELL I'll do it later! (I have to investigate how to assign the proper GPIO
        //We reset the engines
        set_reset(adev,0);
        set_start(adev,0);
        adev->is_started = 0;
        mdelay(100);
        set_reset(adev,1);
        mdelay(100);
        regs[AF_STR_RESET/4]=0xa5;
        regs[AF_TX_RESET/4]=0xa5;
        regs[AF_RX_RESET/4]=0xa5;
        //wait 1/10 second - shouldn't it be in the user space?
        mdelay(100);
        return SUCCESS;
    case ADM_START:
        //First we check if the transfer is not started yet
        if(adev->is_started) {
            printk(KERN_ERR "Acquisition is already started!\n");
            return -EINVAL;
        }
        //Set the buffer numbers
        adev->exp_buf = 0;
        adev->received_buf = 0;
        //Clear the ready flag in all buffers
        for(i=0; i<BUF_NUM; i++) adev->bready[i]=0;
        //We submit request to transfer all buffers
        for(i=0; i<BUF_NUM; i++) transfer_buf(adev,i);
        //And now we enable the interrupts
        regs[AF_IER/4] = 0x04000000;
        mb();
        set_start(adev,1);
        //We should be able to reset the DataMover - both the engine and the STSCMD part.
        //It should be possible to do it under software control!
        adev->is_started = 1;
        return SUCCESS;
    case ADM_STOP:
        //We simply disable the interrupts
        regs[AF_IER/4] = 0x00000000;
        //Here we stop the transfer. How to do it?
        // 1) We stop resending the descriptors
        // 2) We reset the FIFO
        set_start(adev,0);
        set_reset(adev,0);
        adev->is_started = 0;
        return SUCCESS;
    case ADM_GET:
        //We sleep, waiting until the buffer is ready
        res=wait_event_interruptible(readqueue, adev->bready[adev->received_buf] > 0);
        if(res) return res; //Signal received!
        //The buffer is ready to be serviced
        //We return the number of the buffer and the number of available bits
        res = ((adev->received_buf) << 24) | adev->blen[adev->received_buf];
        return res;
    //W tej komendzie powinniśmy oczekiwać na dostępność kolejnego bufora (buforów?)
    //Jeśli bufor jest, to komenda wraca. Powinna być możliwość wysłania z uśpieniem, lub bez.
    //Oprócz tego powinniśmy mieć możliwość korzystania z "poll". Chodzi o to, żeby nie powodować niepotrzebnego
    //mnożenia wątków
    //
    //Jak załatwiamy obsługę błędu "overflow"?
    //Powinniśmy wykrywać problem polegający na tym, że zostały obsłużone wszystkie zlecenia transferu.
    //Oznacza to, że aplikacja nie nadążyła odbierać danych.
    case ADM_CONFIRM:
        //Here we confirm, that we have finished servicing the buffer
        if(adev->bready[adev->received_buf] == 0) return -EINVAL; //Error, the buffer was not ready, we can't confirm it!
        adev->blen[adev->received_buf] = 0;
        adev->bready[adev->received_buf] = 0;
        transfer_buf(adev,adev->received_buf);
        adev->received_buf += 1;
        if(adev->received_buf == BUF_NUM) adev->received_buf = 0;
        return SUCCESS;
    default:
        return -EINVAL;
    }
}


/* Cleanup resources */
void tst1_remove(struct pci_dev *pdev )
{
    axs1_dev * adev = (axs1_dev *) pci_get_drvdata(pdev);
    devs[adev->nr] = NULL;
    if(adev) {
        device_destroy(class_my_tst,MKDEV(MAJOR(my_dev_t),MINOR(my_dev_t)+adev->nr));
        device_destroy(class_my_ctl,MKDEV(MAJOR(my_dev_ctl_t),MINOR(my_dev_ctl_t)+adev->nr));
        axs1_dev_free(adev);
    }
    pci_release_regions(pdev);
    pci_disable_device(pdev);
    //printk("<1>drv_tst1 removed!\n");
}

static int tst1_open(struct inode *inode,
                     struct file *file)
{
    int res=0;
    int nr = MINOR(inode->i_rdev);
    axs1_dev * adev = devs[nr];
    file->private_data = adev;

    nonseekable_open(inode, file);
    res=request_irq(adev->irq,tst1_irq,IRQF_SHARED,DEVICE_NAME,adev);
    if(res) {
        printk (KERN_INFO "wzab_tst1: I can't connect irq %d error: %d\n", adev->irq,res);
        adev->irq_set = 0;
    } else {
        printk (KERN_INFO "wzab_tst1: Connected irq %d\n", adev->irq);
        adev->irq_set = 1;
    }

    return SUCCESS;
}

static int tst1_release(struct inode *inode,
                        struct file *file)
{
    axs1_dev * adev = file->private_data;
    volatile uint32_t * regs = (volatile uint32_t *) adev->fifo_regs;
    if(adev->irq_set > 0) {
        //Stop the core
        //Disable interrupts
        adev->fifo_regs[AF_IER/4] = 0x00000000;
        mb();
        regs[AF_STR_RESET/4]=0xa5;
        regs[AF_TX_RESET/4]=0xa5;
        regs[AF_RX_RESET/4]=0xa5;
        mdelay(100);
        set_start(adev,0);
        set_reset(adev,0);
        mdelay(100);
        set_reset(adev,1);
        //wait 1/10 second - shouldn't it be in the user space?
        adev->is_started = 0;
        free_irq(adev->irq,adev); //Free interrupt
        adev->irq_set = 0;
    }
    file->private_data = NULL;
    return SUCCESS;
}


int tst1_mmap(struct file *filp,
              struct vm_area_struct *vma)
{
    unsigned long vsize;
    unsigned long psize;
    int res;
    axs1_dev * adev = NULL;
    unsigned long off = vma->vm_pgoff;
    adev = (axs1_dev *) filp->private_data;
    vsize = vma->vm_end - vma->vm_start;
    psize = BUF_SIZE;
    printk(KERN_INFO "mmap: vsize=%lx off=%lx\n",vsize,off);
    if((off < 0) || (off >= BUF_NUM)) return -EINVAL;
    vma->vm_pgoff = 0; //We use the offset to pass the number of the buffer
    if(vsize>BUF_SIZE)
        return -EINVAL;
    //#ifdef ARCH_HAS_DMA_MMAP_COHERENT
    printk(KERN_INFO "Mapping with dma_map_coherent DMA buffer at phys: %llx virt %p\n",adev->phys_buf[off],adev->virt_buf[off]);
    res = dma_mmap_coherent(&adev->pdev->dev, vma, adev->virt_buf[off], adev->phys_buf[off], BUF_SIZE);
    if(res==0) {
        printk(KERN_INFO "Mapped DMA buffer at phys: %llx\n",adev->phys_buf[off]);
    }
    return res;
}

static int tst1_probe(struct pci_dev *pdev, const struct pci_device_id *ent)
{
    axs1_dev * adev = NULL;
    int res = 0;
    int i ;
    adev = axs1_dev_alloc();
    if (adev==NULL) {
        dev_err(&pdev->dev, "Can't allocate context for PCI device, aborting\n");
        res = -ENOMEM;
        return res;
    }
    //Check if we still have any free device
    mutex_lock(&devs_lock);
    for ( i = 0 ; i < MY_MAX_DEVICES ; i++ ) {
        if ( devs[i] == NULL ) break;
    }
    if( i >= MY_MAX_DEVICES ) {
        // No more free devices - error
        axs1_dev_free(adev);
        mutex_unlock(&devs_lock);
        goto error_too_many_devs;
    } else {
        adev->nr = i;
        devs[i] = adev;
    };
    mutex_unlock(&devs_lock);
    res =  pci_enable_device(pdev);
    if (res) {
        dev_err(&pdev->dev, "Can't enable PCI device, aborting\n");
        res = -ENODEV;
        goto err1;
    }
    adev->pdev = pdev; //Link adev with the device
    pci_set_drvdata(pdev,adev); //Link device with the adev
    //Now read the resources
    for(i=0; i<N_OF_RES; i++) {
        adev->mmio_start[i] = pci_resource_start(pdev, i*2);
        adev->mmio_end[i] = pci_resource_end(pdev, i*2);
        adev->mmio_flags[i] = pci_resource_flags(pdev, i*2);
        adev->mmio_len[i] = pci_resource_len(pdev, i*2);
        printk(KERN_INFO "Resource: %d start:%llx, end:%llx, flags:%llx, len=%llx\n",
               i,adev->mmio_start[i],adev->mmio_end[i], adev->mmio_flags[i], adev->mmio_len[i]);
        if (!(adev->mmio_flags[i] & IORESOURCE_MEM)) {
            dev_err(&pdev->dev, "region %i not an MMIO resource, aborting\n",i);
            res = -ENODEV;
            goto err1;
        }
    }
    adev->irq = pdev->irq;
    if (!pci_set_dma_mask(pdev, DMA_BIT_MASK(64))) {
        if (pci_set_consistent_dma_mask(pdev, DMA_BIT_MASK(64))) {
            dev_info(&pdev->dev,
                     "Unable to obtain 64bit DMA for consistent allocations\n");
            goto err1;
        }
    }
    //Let's allocate the buffer for BM DMA
    for(i=0 ; i<BUF_NUM ; i++) {
        adev->virt_buf[i] = dma_alloc_coherent(&pdev->dev,BUF_SIZE,&adev->phys_buf[i],GFP_KERNEL);
        if(adev->virt_buf[i]==NULL) {
            printk(KERN_INFO "I can't allocate the DMA buffer nr %d\n",i);
            res = -ENOMEM;
            goto err1;
        }
        printk(KERN_INFO "Allocated DMA buffer at phys: %llx virt: %p\n",adev->phys_buf[i],adev->virt_buf[i]);
    }
    res = pci_request_regions(pdev, DEVICE_NAME);
    if (res)
        goto err1;
    pci_set_master(pdev);
    /* Let's check if the register block is read correctly */
    //Multiply nr by 2 - 64 bit resource!
    adev->pci_regs = pci_iomap(pdev,PCI_REGS*2,adev->mmio_len[PCI_REGS]);
    if(!adev->pci_regs) {
        printk (KERN_ERR "Mapping of memory for %s PCI registers failed\n",
                DEVICE_NAME);
        res= -ENOMEM;
        goto err1;
    }
    printk(KERN_INFO "Mapped %s PCI registers at %p\n",DEVICE_NAME,adev->pci_regs);
    //The first register should return our PCI_ID
    {
        uint32_t pci_id = PCI_VENDOR_ID_WZAB |  (PCI_DEVICE_ID_WZAB_BM1 << 16);
        if(*(adev->pci_regs) != pci_id) {
            dev_info(&pdev->dev, "Not accessible registers BAR? expected id: %x, read id: %x\n",pci_id, *(adev->pci_regs));
            goto err1;
        }
    }
    //Now we can program the location of DMA buffer to the AXIBAR2PCIEBAR
    adev->pci_regs[0x208/4]=0;
    adev->pci_regs[0x20c/4]=0;
    //Map AXI connected registers
    //Multiply nr by 2 - 64 bit resource!
    adev->gpio_regs = pci_iomap(pdev,GPIO_REGS*2,adev->mmio_len[GPIO_REGS]);
    if(!adev->gpio_regs) {
        printk ("<1>Mapping of memory for %s GPIO registers failed\n",
                DEVICE_NAME);
        res= -ENOMEM;
        goto err1;
    }
    printk(KERN_INFO "Mapped %s GPIO registers at %p\n",DEVICE_NAME,adev->gpio_regs);
    //Set the address of the FIFO control registers
    adev->fifo_regs = adev->gpio_regs + 0x10000/4;

    device_create(class_my_tst,&pdev->dev,MKDEV(MAJOR(my_dev_t),MINOR(my_dev_t)+adev->nr),NULL,"ax1dma%d",adev->nr);
    if(res) {
        printk (KERN_ERR "Adding the device for %s failed\n",
                DEVICE_NAME);
        goto err1;
    };
    device_create(class_my_ctl,&pdev->dev,MKDEV(MAJOR(my_dev_ctl_t),MINOR(my_dev_ctl_t)+adev->nr),NULL,"ax1ctl%d",adev->nr);
    if(res) {
        printk (KERN_ERR "Adding the control device for %s failed\n",
                DEVICE_NAME);
        goto err1;
    };
    return 0;
error_too_many_devs:
err1:
    if (adev) axs1_dev_free(adev);
    pci_release_regions(pdev);
    pci_disable_device(pdev);
    return res;
}

static struct pci_driver tst1_pci_driver = {
    .name		= DEVICE_NAME,
    .id_table	= tst1_pci_tbl,
    .probe		= tst1_probe,
    .remove		= tst1_remove,
};

static int __init tst1_init_module(void)
{
    int res;
    /* when a module, this is printed whether or not devices are found in probe */
#ifdef MODULE
    //  printk(version);
#endif
    res=alloc_chrdev_region(&my_dev_t, 0, MY_MAX_DEVICES, DEVICE_NAME);
    if(res) {
        printk (KERN_ERR "Alocation of the device numbers for %s failed\n",
                DEVICE_NAME);
        goto err_register_chdev;
    };
    res=alloc_chrdev_region(&my_dev_ctl_t, 0, MY_MAX_DEVICES, DEVICE_NAME "_ctl");
    if(res) {
        printk (KERN_ERR "Alocation of the control device numbers for %s failed\n",
                DEVICE_NAME);
        goto err_register_chdev;
    };
    //Create the classes
    class_my_tst = class_create(THIS_MODULE, "my_tst_class");
    if (IS_ERR(class_my_tst)) {
        printk(KERN_ERR "Error creating my_tst class.\n");
        res=PTR_ERR(class_my_tst);
        class_my_tst = NULL;
        goto err_create_class;
    }
    class_my_ctl = class_create(THIS_MODULE, "my_ctl_class");
    if (IS_ERR(class_my_ctl)) {
        printk(KERN_ERR "Error creating my_ctl class.\n");
        res=PTR_ERR(class_my_ctl);
        class_my_ctl = NULL;
        goto err_create_class;
    }
    //Create character device for data transfer
    my_cdev = cdev_alloc();
    if(my_cdev == NULL) {
        printk ("<1>Allocation of cdev for %s failed\n",
                DEVICE_NAME);
        goto err_pci_register;
    }
    my_cdev->ops = &Fops;
    my_cdev->owner = THIS_MODULE;
    /* Add character device */
    res=cdev_add(my_cdev, my_dev_t, MY_MAX_DEVICES);
    if(res) {
        printk (KERN_ERR "Registration of the device number for %s failed\n",
                DEVICE_NAME);
        goto err_pci_register;
    };
    //Create character device for control
    my_cdev_ctl = cdev_alloc();
    if(my_cdev_ctl == NULL) {
        printk ("<1>Allocation of cdev ctl for %s failed\n",
                DEVICE_NAME);
        goto err_pci_register;
    }
    my_cdev_ctl->ops = &FopsCtl;
    my_cdev_ctl->owner = THIS_MODULE;
    /* Add character device */
    res=cdev_add(my_cdev_ctl, my_dev_ctl_t, MY_MAX_DEVICES);
    if(res) {
        printk (KERN_ERR "Registration of the control device number for %s failed\n",
                DEVICE_NAME);
        goto err_pci_register;
    };
    res = pci_register_driver(&tst1_pci_driver);
    if(res)
        goto err_pci_register;
    return 0;
err_pci_register:
    if (my_cdev) {
        cdev_del(my_cdev);
        my_cdev = NULL;
    }
    if (my_cdev_ctl) {
        cdev_del(my_cdev_ctl);
        my_cdev_ctl = NULL;
    }
err_create_class:
    if(class_my_tst) class_destroy(class_my_tst);
    class_my_tst = NULL;
    if(class_my_ctl) class_destroy(class_my_ctl);
    class_my_ctl = NULL;
err_register_chdev:
    unregister_chrdev_region(my_dev_t, MY_MAX_DEVICES);
    unregister_chrdev_region(my_dev_ctl_t, MY_MAX_DEVICES);
    my_dev_t = 0;
    return res;
}


static void __exit tst1_cleanup_module(void)
{
    pci_unregister_driver(&tst1_pci_driver);
    if(class_my_tst) {
        class_destroy(class_my_tst);
        class_my_tst=NULL;
    }
    if(class_my_ctl) {
        class_destroy(class_my_ctl);
        class_my_ctl=NULL;
    }
    if(my_dev_t) unregister_chrdev_region(my_dev_t, MY_MAX_DEVICES);
    my_dev_t = 0;
    if(my_dev_ctl_t) unregister_chrdev_region(my_dev_ctl_t, MY_MAX_DEVICES);
    my_dev_ctl_t = 0;
}


module_init(tst1_init_module);
module_exit(tst1_cleanup_module);

